这是团队公开文档，没有什么有趣的信息
你访问这里可能是因为你收到了加入团队的邀请。

你需要做如下事情：

1. 注册加入bitbucket
1. 试着访问[团队私有文档](https://bitbucket.org/team-zhuangjm/teamdoc)
1. 如果无法访问团队私有文档，应该是你并没有使用**邀请信的email地址**注册bitbucket，请告知你的bitbucket用户名，我会将你加入团队